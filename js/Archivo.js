/**
 * Lee un archivo
 */
var leerArchivo = function() {
    var files = document.getElementById('AbrirArchivo').files;
    if (!files.length) {
        return;
    }
    var file = files[0];
    var reader = new FileReader();
    // If we use onloadend, we need to check the readyState.
    reader.onloadend = function(evt) {
        var nuevaRed = JSON.parse(evt.target.result);
        ControladorRed.definirRed(nuevaRed);
    };
    var blob = file.slice(0, file.size);
    reader.readAsBinaryString(blob);
};

$(document).ready(function() {
    $("#Importar").click(leerArchivo);
    $("#Exportar").click(function() {
        saveAs(
                new Blob(
                        [JSON.stringify(ControladorRed.obtenerRed())]
                        , {type: "application/json;charset=" + document.characterSet}
                )
                , "modelo_red.json"
                );
    });

});

