/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var Corriente = function(corriente, redOriginal) {
    var red = redOriginal.crearCopia();
    var encontrarCorriente = function() {
        var lista = red.Elementos;
        for (var index in lista) {
            var generador = lista[index];
            if (generador.tipo === "generador") {
                var V= math.complex( generador.V);
                var Sg = math.complex(generador.Sg);
                generador.I = math.divide( math.conj(Sg),math.conj(V));

            }
        }
    };
    encontrarCorriente();
    return red;
};
