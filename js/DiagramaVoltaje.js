/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var DiagramaVoltaje = function(redOriginal, Vn) {
    var red=redOriginal.crearCopia();
    var encontarVoltajes = function() {
        var equilibrio = red.obtenerEquilibrio();
        var Ve = red.Ve();
        var VePolar = Ve.toPolar();
        var equilibrioEncontrado = 0;
        var indexBarraActual = 0;
        var lista = red.Elementos;
        for (var index in lista) {
            var barra = lista[index];
            if (barra.tipo === "barra") {
                if (indexBarraActual === equilibrio) {
                    barra.V = Ve;
                    barra.I = VePolar.r;
                    barra.J = VePolar.phi;
                    equilibrioEncontrado = 1;
                } else {
                    barra.V =  Vn._data[indexBarraActual - equilibrioEncontrado][0];
                    var VnPolar = barra.V.toPolar();
                    barra.I = VnPolar.r;
                    barra.J = VnPolar.phi;

                }
                indexBarraActual++;
            }
        }
        for (var index in lista) {
            var generador = lista[index];
            if (generador.tipo === "generador" ) {
                var barraConnectada = red.obtenerConectados(generador)[0];
                generador.V = barraConnectada.V;
            }
        }


    };
    encontarVoltajes();
    return red;
};
