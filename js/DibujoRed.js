var DibujardorRed = function(laCanva, elTipo) {
    var canva = laCanva;
    var tipo = elTipo;//red,potencia,voltaje
    var ctx = canva.getContext("2d");

    var red;
    var elementoActivo = null;


    /*
     * Metodos de dibujo en canva
     */

    var ColorRadial = '#9999FF';

    var colorSeleccion = function(elemento, verificar) {
        if (verificar && !red.estatus(elemento).valido) {
            if (elemento === elementoActivo) {
                ctx.strokeStyle = '#AA5555';
                ctx.fillStyle = "#AA5555";
            } else {
                ctx.strokeStyle = '#BB3333';
                ctx.fillStyle = "#BB3333";
            }
        } else if (elemento === elementoActivo && elemento.equilibrio) {
            ctx.strokeStyle = '#AA9977';
            ctx.fillStyle = "#AA9977";
        } else if (elemento === elementoActivo) {
            ctx.strokeStyle = '#889977';
            ctx.fillStyle = "#889977";
        } else if (elemento.equilibrio) {
            ctx.strokeStyle = '#CC9977';
            ctx.fillStyle = "#CC9977";
        } else if (elemento.radial) {
            ctx.strokeStyle = ColorRadial;
            ctx.fillStyle = ColorRadial;
        } else {
            ctx.fillStyle = "#000000";
            ctx.strokeStyle = '#000000';
        }

    };

    var dibjuarNumero = function(index, centroXNumero, centroYNumero) {
        //Dibujar N�mero
        ctx.fillText(index, centroXNumero, centroYNumero);
        //El c�rculo
        ctx.beginPath();
        ctx.arc(centroXNumero + 9, centroYNumero - 7, 13, 0, 2 * Math.PI);
        ctx.stroke();
    };


    var dibujarLinea = function(linea) {
        var coordendas = red.coordendasLinea(linea);
        ctx.beginPath();
        ctx.moveTo(coordendas.origen.x, coordendas.origen.y);
        ctx.lineTo(coordendas.destino.x, coordendas.destino.y);
        ctx.stroke();
        var XMed = (coordendas.origen.x + coordendas.destino.x) / 2 + 10;
        var YMed = (coordendas.origen.y + coordendas.destino.y) / 2;
        var origen = red.obtenerElmento(linea.id_origen);
        var destino = red.obtenerElmento(linea.id_destino);
        if (origen.tipo !== "barra" || destino.tipo !== "barra") {
            return;
        }
        if (tipo === "red") {
            ctx.fillText("R: " + redondear(linea.R), XMed, YMed);
            ctx.fillText("X: " + linea.X, XMed, YMed + 20);
        } else if (tipo === "flujo") {
            var Sij = linea.Sij;
            if (Sij.re < 0) {
                Sij.re = -Sij.re;
                Sij.im = -Sij.im;
                var temp = coordendas.origen;
                coordendas.origen = coordendas.destino;
                coordendas.destino = temp;
            }
            if (coordendas.origen.y > coordendas.destino.y) {
                dibujarCarga({
                    x: XMed,
                    y: YMed,
                    P: Sij.re,
                    Q: Sij.im
                });
            } else {
                dibujarGenerador({
                    x: XMed,
                    y: YMed - 20,
                    P: Sij.re,
                    Q: Sij.im
                });
            }
        }
    };


    var dibujarCarga = function(carga, index) {
        var centroXNumero = carga.x;
        var centroYNumero = carga.y - 15;
        //El generador
        ctx.beginPath();

        ctx.moveTo(centroXNumero, centroYNumero - 10);
        ctx.lineTo(centroXNumero + 5, centroYNumero + 10);
        ctx.lineTo(centroXNumero - 5, centroYNumero + 10);
        ctx.lineTo(centroXNumero, centroYNumero - 10);
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(centroXNumero, centroYNumero + 8);
        ctx.lineTo(centroXNumero, centroYNumero + 30);
        ctx.stroke();
        if (carga.dibujarBarraCarga) {
            ctx.beginPath();
            ctx.moveTo(centroXNumero - 16, centroYNumero + 18);
            ctx.lineTo(centroXNumero + 16, centroYNumero + 18);
            ctx.stroke();
        }
        if (tipo === "red" || tipo === "flujo") {
            ctx.fillText(generarTextoReImg(carga.P, carga.Q) + " MVA", centroXNumero + 15, centroYNumero + 10);
        }
        var corrienteInicial = carga.corrienteInicial;
        if (typeof corrienteInicial !== 'undefined') {
            //Es red temporarl
            ctx.fillText("I= " + generarTextoComplejo(corrienteInicial), centroXNumero + 20, centroYNumero + 35);
        }
    };

    var dibujarBarra = function(barra, index) {
        var centroXNumero = barra.x + barra.largo;
        var centroYNumero = barra.y;
        //Crear barra
        ctx.fillRect(barra.x - 10, barra.y - 5, barra.largo + 20, 7);
        if (barra.tieneNombre) {
            dibjuarNumero(index, centroXNumero + 20, centroYNumero + 5);
        }
        // alert(barra.P);
        if (tipo === "red") {
            if (barra.tieneNombre) {
                ctx.fillText(barra.nombre, centroXNumero + 48, centroYNumero + 5);
            }
        } else if (tipo === "potencia") {
            if (!barra.equilibrio) {
                ctx.fillText(generarTextoReImg(barra.P, barra.Q) + " p.u.", centroXNumero + 48, centroYNumero + 5);
            }
        } else if (tipo === "voltaje") {
            ctx.fillText("V= " + redondear(barra.I) + " \u2220 " + redondear(aDegrados(barra.J)) + "�", centroXNumero + 48, centroYNumero + 5);
        }

        var corrienteInicial = barra.corrienteInicial;
        if (typeof corrienteInicial !== 'undefined') {
            //Es red temporarl

            ctx.fillStyle = "#000000";
            ctx.strokeStyle = '#000000';
            ctx.fillText("I= " + generarTextoComplejo(corrienteInicial), centroXNumero + 20, centroYNumero + 10);
        }
    };

    var dibujarTransformador = function(transformador, index) {
        var centroXNumero = transformador.x;
        var centroYNumero = transformador.y;
        //El transformador
        ctx.beginPath();
        ctx.arc(centroXNumero, centroYNumero - 6, 13, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(centroXNumero, centroYNumero + 6, 13, 0, 2 * Math.PI);
        ctx.stroke();
        var desplazo = -10;

        ctx.fillText("R= " + transformador.R, centroXNumero + 15, centroYNumero + 23);

        ctx.fillText("X= " + transformador.X, centroXNumero + 15, centroYNumero - 3 + desplazo);
        ctx.fillText("S= " + transformador.S + " MVA", centroXNumero + 15, centroYNumero + 16 + desplazo);
        var corrienteInicial = transformador.corrienteInicial;
        if (typeof corrienteInicial !== 'undefined') {
            //Es red temporarl
            //ctx.fillText("I= " + generarTextoComplejo(corrienteInicial), centroXNumero + 20, centroYNumero + 35);
        }
    };

    var dibujarGenerador = function(generador, index) {
        var textoX;
        var textoY;
        if (generador.dibujarGenerador) {
            var centroXNumero = generador.x;
            var centroYNumero = generador.y;
            //El generador
            ctx.beginPath();
            ctx.arc(centroXNumero, centroYNumero - 7, 15, 0, 2 * Math.PI);
            ctx.stroke();

            ctx.beginPath();
            ctx.arc(centroXNumero - 5, centroYNumero - 7, 5, Math.PI - 0.5, 2 * Math.PI);
            ctx.stroke();
            ctx.beginPath();
            ctx.arc(centroXNumero + 5, centroYNumero - 7, 5, -0.5, Math.PI);
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(centroXNumero, centroYNumero + 8);
            ctx.lineTo(centroXNumero, centroYNumero + 30);
            ctx.stroke();
            ctx.beginPath();
            ctx.moveTo(centroXNumero - 16, centroYNumero + 18);
            ctx.lineTo(centroXNumero + 16, centroYNumero + 18);
            ctx.stroke();
            textoX = centroXNumero + 15;
            textoY = centroYNumero;

        } else {
            var centroXNumero = generador.x;
            var centroYNumero = generador.y - 15;
            //El generador
            ctx.beginPath();

            ctx.moveTo(centroXNumero, centroYNumero + 47);
            ctx.lineTo(centroXNumero + 5, centroYNumero + 30);
            ctx.lineTo(centroXNumero - 5, centroYNumero + 30);
            ctx.lineTo(centroXNumero, centroYNumero + 47);
            ctx.fill();

            ctx.beginPath();
            ctx.moveTo(centroXNumero, centroYNumero + 8);
            ctx.lineTo(centroXNumero, centroYNumero + 30);
            ctx.stroke();
            textoX = centroXNumero + 5;
            textoY = centroYNumero + 20;

        }
        if (tipo === "red") {
            ctx.fillText(generarTextoReImg(generador.P, generador.Q) + " MVA", textoX, textoY);
            var corrienteInicial = generador.corrienteInicial;
            if (typeof corrienteInicial !== 'undefined') {
                //Es red temporarl
                ctx.fillText("I= " + generarTextoComplejo(corrienteInicial), textoX + 10, textoY + 20);
            }
        } else if (tipo === "flujo") {
            ctx.fillText(generarTextoReImg(generador.P, generador.Q) + " MVA", textoX, textoY);
        } else if (tipo === "corriente") {
            ctx.fillText("I= " + generarTextoComplejo(generador.I) , textoX, textoY);
        }else if (tipo === "electromotriz") {
            ctx.fillText("E = "+generarTextoComplejo(generador.E ), textoX+10, textoY - 5);
            ctx.fillText("E' = "+generarTextoComplejo(generador.EPrima) , textoX+7, textoY+13);
            ctx.fillText("E'' = "+generarTextoComplejo(generador.EPrimaPrima)  , textoX+4, textoY+31);
        }

    };



    var dibujarElemento = function(elemento, index, numeroBarra) {
        var tipo = elemento.tipo;
        if (tipo === "barra") {
            dibujarBarra(elemento, numeroBarra);
        } else if (tipo === "transformador") {
            dibujarTransformador(elemento, index);
        } else if (tipo === "generador") {
            dibujarGenerador(elemento, index);
        } else if (tipo === "carga") {
            dibujarCarga(elemento, index);
        } else if (tipo === "linea") {
            dibujarLinea(elemento);
        }
    };




    this.dibujarRed = function(laRed, elElementoActivo, verificar) {
        red = laRed;
        elementoActivo = elElementoActivo;
        ctx.clearRect(0, 0, canva.width, canva.height);
        ctx.font = "18px Arial";
        var numeroBarra = 0;
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                numeroBarra++;
            }
            colorSeleccion(elemento, verificar);
            dibujarElemento(elemento, index, numeroBarra);
        }
    };

};
