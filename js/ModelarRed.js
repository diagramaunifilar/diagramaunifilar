/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var ControladorRed;

var ControlRed = function() {
    //Variables iniciales
    var esteControlador = this;
    var agregandoLinea = false;
    var red = Red.nuevaRed();

    //Manejo de la canva
    var redModelada = "RedModelada";
    var canvaId = "#" + redModelada;
    //Var se puede ver como privado, ademas quitamos todos los this
    var dibujardorRedModelada = new DibujardorRed(document.getElementById(redModelada), "red");
    var dibujardorRedSimplificada = new DibujardorRed(document.getElementById("RedSimplificada"), "red");
    var dibujardorEsquemaPotencia = new DibujardorRed(document.getElementById("EsquemaPotencia"), "potencia");
    var dibujardorDiagramaVoltaje = new DibujardorRed(document.getElementById("DiagramaVoltaje"), "voltaje");
    var dibujardorFlujoCarga = new DibujardorRed(document.getElementById("FlujoCarga"), "flujo");
    var dibujardorCorriente = new DibujardorRed(document.getElementById("Corriente"), "corriente");
    var dibujardorTensionElectromotriz = new DibujardorRed(document.getElementById("TesionElemetromotriz"), "electromotriz");

    //Resistencia
    var R = $("#R");
    //Reactancia
    var X = $("#X");


    //P + jQ del generador
    var genP = $("#genP");
    var genQ = $("#genQ");
    var genXd = $("#genXd");
    var genXdPrima = $("#genXdPrima");
    var genXdPrimaPrima = $("#genXdPrimaPrima");
    var genDibujar = $("#genDibujar");

    //P + jQ de la carga
    var cargaP = $("#cargaP");
    var cargaQ = $("#cargaQ");
    var cargaDibujar = $("#cargaDibujar");

    //Nombre de la barra
    var nombre = $("#nombre");
    var barraEquilibrio = $("#barraEquilibrio");
    var barraTieneNombre = $("#barraTieneNombre");
    var datosBarraEquilibrio = $("#datosBarraEquilibrio");
    var equilibrioXd = $("#equilibrioXd");
    var equilibrioXdPrima = $("#equilibrioXdPrima");
    var equilibrioXdPrimaPrima = $("#equilibrioXdPrimaPrima");


    //Potencia completa y reactancia del transformador
    var transS = $("#transS");
    var transX = $("#transX");
    var transR = $("#transR");


    //Manejo de interfaz de simplificar
    var simplificarOriginal = $("#simplificarOriginal");
    var simplificarAnterior = $("#simplificarAnterior");
    var simplificarSiguiente = $("#simplificarSiguiente");
    var simplificarFinal = $("#simplificarFinal");
    var etapa = $("#etapa");
    var redesSimplificar;
    var indiceRedSimplicadaActual;

    var tabActual;


    //Elementos externos
    var elementoActivo = null;
    var elementoActivoIndex = null;


    var dibujarRed = function() {
        dibujardorRedModelada.dibujarRed(red, elementoActivo, red_verificar.prop('checked'));
        seleccionarActivo();
        if (tabActual === 1) {
            $("#simplificar").show();
        }
    };

    var red_verificar = $("#red_verificar");
    red_verificar.click(dibujarRed);


    /**
     * Variables globales
     */
    var red_basePU = $("#red_basePU");
    var red_tensionPU = $("#red_tensionPU");
    var red_tensionAngulo = $("#red_tensionAngulo");
    var red_Actualizar = $("#red_Actualizar");


    var red_epsilon = $("#red_epsilon");
    var red_max_iter = $("#red_max_iter");
    red_Actualizar.click(function() {
        red.Configuracion.basePU = parseFloat(red_basePU.val());
        red.Configuracion.tensionPU = parseFloat(red_tensionPU.val());
        red.Configuracion.tensionAngulo = parseFloat(aRadianes(red_tensionAngulo.val()));
        red.Configuracion.epsilon = parseFloat(red_epsilon.val());
        red.Configuracion.max_iter = parseFloat(red_max_iter.val());
        mostrarCorrienteBase();
    });

    var mostrarCorrienteBase = function() {
        $("#red_corrienteBase").html(generarTextoComplejo(red.corrienteBase()));
    };

    /*
     * Procesamiento de la red
     */
    var seleccionarElemento = function(index) {
        elementoActivoIndex = index;
        elementoActivo = red.Elementos[index];
        dibujarRed();
    };
    var agregarElemento = function(tipo, x, y) {
        var index = red.Elementos.length;
        var nuevaElemento = red.agregarElemento(tipo, x, y);
        if (tipo === "barra") {
            nuevaElemento.largo = 200;
            nuevaElemento.nombre = "";
            nuevaElemento.equilibrio = false;
            nuevaElemento.tieneNombre = false;
            nuevaElemento.X = 0;
            nuevaElemento.S = 0;
            nuevaElemento.R = 0;
            nuevaElemento.equilibrioXd = 0;
            nuevaElemento.equilibrioXdPrima = 0;
            nuevaElemento.equilibrioXdPrimaPrima = 0;
        }
        if (tipo === "linea") {
            nuevaElemento.R = 0;
            nuevaElemento.X = 0;
        }
        if (tipo === "generador") {
            nuevaElemento.P = 0;
            nuevaElemento.Q = 0;
            nuevaElemento.Xd = 0;
            nuevaElemento.XdPrima = 0;
            nuevaElemento.XdPrimaPrima = 0;
            nuevaElemento.dibujarGenerador = 0;
        }
        if (tipo === "transformador") {
            nuevaElemento.R = 0;
            nuevaElemento.X = 0;
            nuevaElemento.S = 0;
        }
        if (tipo === "carga") {
            nuevaElemento.P = 0;
            nuevaElemento.Q = 0;
            nuevaElemento.dibujarBarraCarga = true;
        }
        seleccionarElemento(index);
    };



    var agregarLinea = function(id_destino) {
        red.agregarLinea(elementoActivo.id, id_destino, 0, 0);
        dibujarRed();
        agregandoLinea = false;
    };


    var coordendasElemento = function(elemento) {
        if (elemento.tipo === "linea") {
            return red.coordendasLinea(elemento);
        }
        var largo = 0;
        if (elemento.tipo === "barra") {
            largo = elemento.largo + 30;
        }
        return {
            origenX: elemento.x - 20,
            origenY: elemento.y - 20,
            destinoX: elemento.x + 20 + largo,
            destinoY: elemento.y + 20
        };
    };





    var desactivarBarras = function() {
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra" && elemento !== elementoActivo) {
                elemento.equilibrio = false;
            }
        }
    };

    /*
     * Metodos publicos
     * */
    this.definirRed = function(nuevaRed) {
        red = Red(nuevaRed);
        dibujarRed();
        red_basePU.val(red.Configuracion.basePU);
        red_tensionPU.val(red.Configuracion.tensionPU);
        red_tensionAngulo.val(aDegrados(red.Configuracion.tensionAngulo));
        red_epsilon.val(red.Configuracion.epsilon);
        red_max_iter.val(red.Configuracion.max_iter);
        mostrarCorrienteBase();
    };

    this.obtenerRed = function() {
        return red;
    };


    var seleccionarActivo = function() {
        $("#todos").children().hide();
        if (elementoActivo !== null) {
            $("#mostrar").show();
            if (elementoActivo.equilibrio) {
                $("#transformador").show();
                $("#equilibrio").show();
                datosBarraEquilibrio.show();
            } else {
                $("#equilibrio").hide();
                datosBarraEquilibrio.hide();
            }
            $("#" + elementoActivo.tipo).show();
            if ("linea" === elementoActivo.tipo) {
                var origen = red.obtenerElmento(elementoActivo.id_origen);
                var destino = red.obtenerElmento(elementoActivo.id_destino);
                if (origen.tipo !== "barra" || destino.tipo !== "barra") {
                    $("#linea").hide();
                } else {
                    X.val(elementoActivo.X);
                    R.val(elementoActivo.R);
                }
            }
            if ("generador" === elementoActivo.tipo) {
                genP.val(elementoActivo.P);
                genQ.val(elementoActivo.Q);
                genXd.val(elementoActivo.Xd);
                genXdPrima.val(elementoActivo.XdPrima);
                genXdPrimaPrima.val(elementoActivo.XdPrimaPrima);
                genDibujar.prop('checked', elementoActivo.dibujarGenerador);
            }
            if ("carga" === elementoActivo.tipo) {
                cargaP.val(elementoActivo.P);
                cargaQ.val(elementoActivo.Q);
                cargaDibujar.prop('checked', elementoActivo.dibujarBarraCarga);
            }
            if ("barra" === elementoActivo.tipo) {
                nombre.val(elementoActivo.nombre);
                barraEquilibrio.prop('checked', elementoActivo.equilibrio);
                barraTieneNombre.prop('checked', elementoActivo.tieneNombre);
                transS.val(elementoActivo.S);
                transX.val(elementoActivo.X);
                transR.val(elementoActivo.R);
                equilibrioXd.val(elementoActivo.equilibrioXd);
                equilibrioXdPrima.val(elementoActivo.equilibrioXdPrima);
                equilibrioXdPrimaPrima.val(elementoActivo.equilibrioXdPrimaPrima);

            }
            if ("transformador" === elementoActivo.tipo) {
                transS.val(elementoActivo.S);
                transX.val(elementoActivo.X);
                transR.val(elementoActivo.R);
            }
        }

    };

    this.actualizaElemento = function() {
        if (elementoActivo !== null) {
            if (elementoActivo.tipo === "linea") {
                elementoActivo.X = parseFloat(X.val());
                elementoActivo.R = parseFloat(R.val());
            }
            if (elementoActivo.tipo === "generador") {
                elementoActivo.P = parseFloat(genP.val());
                elementoActivo.Q = parseFloat(genQ.val());
                elementoActivo.Xd = parseFloat(genXd.val());
                elementoActivo.XdPrima = parseFloat(genXdPrima.val());
                elementoActivo.XdPrimaPrima = parseFloat(genXdPrimaPrima.val());
                elementoActivo.dibujarGenerador = genDibujar.prop('checked');
            }
            if (elementoActivo.tipo === "carga") {
                elementoActivo.P = parseFloat(cargaP.val());
                elementoActivo.Q = parseFloat(cargaQ.val());
                elementoActivo.dibujarBarraCarga = cargaDibujar.prop('checked');
            }
            if (elementoActivo.tipo === "transformador") {
                elementoActivo.X = parseFloat(transX.val());
                elementoActivo.S = parseFloat(transS.val());
                elementoActivo.R = parseFloat(transR.val());
            }
            if (elementoActivo.tipo === "barra") {
                elementoActivo.nombre = nombre.val();
                elementoActivo.equilibrio = barraEquilibrio.prop('checked');
                elementoActivo.tieneNombre = barraTieneNombre.prop('checked');
                if (elementoActivo.equilibrio) {
                    desactivarBarras();
                }
                elementoActivo.X = parseFloat(transX.val());
                elementoActivo.S = parseFloat(transS.val());
                elementoActivo.R = parseFloat(transR.val());
                elementoActivo.equilibrioXd = parseFloat(equilibrioXd.val());
                elementoActivo.equilibrioXdPrima = parseFloat(equilibrioXdPrima.val());
                elementoActivo.equilibrioXdPrimaPrima = parseFloat(equilibrioXdPrimaPrima.val());
            }
        }
        dibujarRed();
    };

    this.desactivarElemento = function() {
        elementoActivo = null;
        elementoActivoIndex = null;
        dibujarRed();
    };

    this.eliminarElemento = function() {
        if (elementoActivo === null) {
            return;
        }
        red.eliminarElemento(elementoActivo);
        elementoActivo = null;
        seleccionarActivo();
    };


    this.inicioLinea = function() {
        this.desactivarElemento();
        agregandoLinea = true;
    };

    this.procesarClick = function(x, y, tipo) {
        var linea = agregandoLinea && elementoActivo !== null;
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            var coordenadas = coordendasElemento(elemento);
            if (x >= coordenadas.origenX && x <= coordenadas.destinoX &&
                    y >= coordenadas.origenY && y <= coordenadas.destinoY) {
                if (linea) {
                    agregarLinea(elemento.id);
                } else {
                    if (!(elemento.tipo === "linea" && agregandoLinea)) {
                        seleccionarElemento(index);
                    }
                }
                return;
            }
        }
        agregarElemento(tipo, x, y);
    };

    var moverXY = function(x, y) {
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo !== "linea") {
                elemento.x = Math.max(0, elemento.x + x);
                elemento.y = Math.max(0, elemento.y + y);
                elemento.x = Math.min(2000, elemento.x + x);
                elemento.y = Math.min(2000, elemento.y + y);
            }
        }
    };

    this.processKey = function(key, isShift) {

        if (document.activeElement.type === "text") {
            return true;
        }
        if (isShift) {
            if (key === 38) {
                moverXY(0, -10);
            } else if (key === 37) {
                moverXY(-10, 0);
            } else if (key === 39) {
                moverXY(10, 0);
            } else if (key === 40) {
                moverXY(0, 10);
            }
        } else {
            if (elementoActivo === null) {
                return true;
            }
            if (key === 38) {
                elementoActivo.y = Math.max(0, elementoActivo.y - 10);
            } else if (key === 37) {
                elementoActivo.x = Math.max(0, elementoActivo.x - 10);
            } else if (key === 39) {
                elementoActivo.x = Math.min(2000, elementoActivo.x + 10);
            } else if (key === 40) {
                elementoActivo.y = Math.min(2000, elementoActivo.y + 10);
            }///Delete
            else if (key === 46) {
                this.eliminarElemento();
            } else if (key === 107 || key === 171) {
                if (elementoActivo.tipo === "barra") {
                    elementoActivo.largo = Math.min(2000, elementoActivo.largo + 10);
                }
            } else if (key === 109 || key === 173) {
                if (elementoActivo.tipo === "barra") {
                    elementoActivo.largo = Math.max(40, elementoActivo.largo - 10);
                }
            } else {
                return true;
            }
        }

        //Summar or restar tama�� barra


        dibujarRed();
        return false;
    };

    var activarVerficiacion = function(razon) {
        alert(razon);
        if (!$("#red_verificar").prop('checked')) {
            $("#red_verificar").click();
        }
    };

    var dibujarRedSimplificada = function() {
        etapa.html("Etapa "+indiceRedSimplicadaActual+" de "+(redesSimplificar.length-1));
        var redSimplificada = redesSimplificar[indiceRedSimplicadaActual];
        dibujardorRedSimplificada.dibujarRed(redSimplificada);
    };

    this.seleccionarTab = function(tab) {
        tabActual = tab;
        elementoActivo = null;
        elementoActivoIndex = null;
        dibujarRed();

    };

    this.generarEsquemaPotencia = function() {
        var estatus = VerificarRed(red, false);
        if (!estatus.valido) {
            return activarVerficiacion(estatus.razon);
        }
        redesSimplificar = Simplificar(red);
        indiceRedSimplicadaActual = 0;
        var redSimplificadaFinal = redesSimplificar[redesSimplificar.length - 1];
        estatus = VerificarRed(redSimplificadaFinal, true);
        if (!estatus.valido) {
            return activarVerficiacion(estatus.razon);
        }
        dibujarRedSimplificada();

        //Potencia aparente - ya no necesitamos cargas
        var potenciaAparente = PotenciaAparente(redSimplificadaFinal).removerElementos("carga");
        //Se remueven los generadores solo para el dibujo
        dibujardorEsquemaPotencia.dibujarRed(potenciaAparente.crearCopia().removerElementos("generador"));



        //Generacion y desplegar regimen permanente
        var regimenPermanente = new RegimenPermanente(potenciaAparente);

        var impedancia = regimenPermanente.impedancia();
        var impedanciaHtml = regimenPermanente.generarHTMLMatriz(impedancia);
        $("#matrizImpedancia").html(impedanciaHtml);

        var admitancia = regimenPermanente.admitancia();
        var admitanciaHtml = regimenPermanente.generarHTMLMatriz(admitancia);
        $("#matrizAdmitancia").html(admitanciaHtml);

        var YnnInv = regimenPermanente.YnnInv();
        var YnnInvHtml = regimenPermanente.generarHTMLMatrizConEquilibrio(YnnInv);
        $("#YnnInv").html(YnnInvHtml);
        var Yne = regimenPermanente.Yne();
        var YneHtml = regimenPermanente.generarHTMLVector(Yne, true);
        $("#Yne").html(YneHtml);

        var SnConj = regimenPermanente.SnConj();
        var SnConjHtml = regimenPermanente.generarHTMLVector(SnConj, true);
        $("#SnConj").html(SnConjHtml);

        var Vn = regimenPermanente.Vn();
        var iteracionEstable = regimenPermanente.iteracionEstable();
        var VnHtml = regimenPermanente.generarHTMLVector(Vn, true);
        $("#Vn").html(VnHtml);
        $("#VnIter").html("Iteracion estable " + iteracionEstable);

        var Se = regimenPermanente.Se();
        $("#Se").html(generarTextoComplejo(Se));

        var voltajesConj = regimenPermanente.voltajesConj();
        var voltajesConjHtml = regimenPermanente.generarHTMLVector(voltajesConj, false);
        $("#conjVoltajes").html(voltajesConjHtml);

        var potenciaComplejaConj = regimenPermanente.potenciaComplejaConj();
        var potenciaComplejaConjHtml = regimenPermanente.generarHTMLVector(potenciaComplejaConj, false);
        $("#potenciaComplejaConj").html(potenciaComplejaConjHtml);

        var corriente = regimenPermanente.corriente();
        var corrienteHtml = regimenPermanente.generarHTMLVector(corriente, false);
        $("#TablaCorriente").html(corrienteHtml);

        var Sij = regimenPermanente.Sij();
        var SijHtml = regimenPermanente.generarHTMLMatriz(Sij);
        $("#Sij").html(SijHtml);

        $("#Perdidas").html(generarTextoComplejo(regimenPermanente.perdidasPotencia()));


        //Dibujar diagrama de voltaje
        var diagramaVoltajeYFlujo = DiagramaVoltaje(potenciaAparente, Vn);
        var voltajeYFlujoSinGeneradoresNiCargas = diagramaVoltajeYFlujo.crearCopia().removerElementos("generador");
        dibujardorDiagramaVoltaje.dibujarRed(voltajeYFlujoSinGeneradoresNiCargas);

        //Dibujar el flujo
        //Solo para dibujar
        dibujardorFlujoCarga.dibujarRed(voltajeYFlujoSinGeneradoresNiCargas.agregarGeneradorBarraEquilibrio(true));

        //dibujar la corriente
        var redCorriente = Corriente(corriente,diagramaVoltajeYFlujo.agregarGeneradorBarraEquilibrio(false));
        dibujardorCorriente.dibujarRed(redCorriente);

        var tensionElectromotriz = TensionElectromotriz(redCorriente);
        dibujardorTensionElectromotriz.dibujarRed(tensionElectromotriz);
    };



    var redondearA10 = function(x) {
        return ((Math.round(x / 10.0)) * 10);
    };

    /*
     * Metodos auxiliares
     */

    $(canvaId).click(function(e) {
        var offset = $(this).offset();
        var x = redondearA10(e.clientX - offset.left);
        var y = redondearA10(e.clientY - offset.top);
        var tipo = $("#tipo_elemento").val();
        esteControlador.procesarClick(x, y, tipo);
    });

    $("#elemento_Actualizar").click(function() {
        esteControlador.actualizaElemento();
    });

    $("#elemento_Eliminar").click(function() {
        esteControlador.eliminarElemento();

    });
    $("#agregar_Linea").click(function() {
        esteControlador.inicioLinea();
    });
    $("#RedBasica").click(function() {
        $.ajax({
            type: "GET",
            //url: "./Redes/red_base.json",
            url: "./Redes/red_tarea.json",
            dataType: "json",
            success: function(data) {
                esteControlador.definirRed(data);
                // createImage = data.createImage;
            }
        });
    });
    $("#RedTesis").click(function() {
        $.ajax({
            type: "GET",
            url: "./Redes/red_tesis.json",
            dataType: "json",
            success: function(data) {
                esteControlador.definirRed(data);
                // createImage = data.createImage;
            }
        });
    });
    $("#EsquemaTesis").click(function() {
        $.ajax({
            type: "GET",
            url: "./Redes/esquema_tesis.json",
            dataType: "json",
            success: function(data) {
                esteControlador.definirRed(data);
                // createImage = data.createImage;
            }
        });
    });

    simplificarOriginal.click(function() {
        indiceRedSimplicadaActual = 0;
        dibujarRedSimplificada();
    });

    simplificarAnterior.click(function() {
        var i = math.max(indiceRedSimplicadaActual - 1, 0);
        indiceRedSimplicadaActual = i;
        dibujarRedSimplificada();
    });

    simplificarSiguiente.click(function() {
        var i = math.min(indiceRedSimplicadaActual + 1, redesSimplificar.length - 1);
        indiceRedSimplicadaActual = i;
        dibujarRedSimplificada();
    });

    simplificarFinal.click(function() {
        indiceRedSimplicadaActual = redesSimplificar.length - 1;
        dibujarRedSimplificada();
    });


    $("#generarEsquemaPotencia").click(function() {
        esteControlador.generarEsquemaPotencia();
    });

    document.onkeydown = function(e) {
        var keyPress;
        var isShift;
        if (typeof event !== 'undefined') {
            keyPress = event.keyCode;
            isShift = event.shiftKey ? true : false;
        }
        else if (e) {
            keyPress = e.which;
            isShift = e.shiftKey ? true : false;
        }
        //if (isShift)
        //alert(keyPress);
        return esteControlador.processKey(keyPress, isShift);

    };

};


$(document).ready(function() {
    ControladorRed = new ControlRed();
    $("#todos").children().hide();
    ControladorRed.definirRed(Red.nuevaRed());

    $('#tabs').bind('tabsshow', function(event, ui) {
        alert(ui.index);
        switch (ui.index) {
            case 0:
                $('body').css('background-image', '/images/backgrounds/1.jpg');
                break;
        }
    });
});