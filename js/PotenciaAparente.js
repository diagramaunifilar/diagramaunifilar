/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var PotenciaAparente = function(redOriginal) {
    var red =redOriginal.crearCopia();
    var obtenerPQ = function(elemento) {
        var PQ = {P:0,Q:0};
        var connectados = red.obtenerConectados(elemento);
        for (var index in connectados) {
            var elementoConnectado = connectados[index];
            if (elementoConnectado.tipo === "generador") {
                PQ.P += elementoConnectado.P;
                PQ.Q += elementoConnectado.Q;
            }
            if (elementoConnectado.tipo === "carga") {
                PQ.P -= elementoConnectado.P;
                PQ.Q -= elementoConnectado.Q;
            }
            if (elementoConnectado.tipo === "transformador") {
                PQ = obtenerPQ(elementoConnectado);
            }
        }
        return PQ;
    };

    var agregarPU = function() {
        var basePU = red.Configuracion.basePU;
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                var PQ = obtenerPQ(elemento);
                elemento.P = PQ.P/basePU;
                elemento.Q = PQ.Q/basePU;
            }
        }
    };
    agregarPU();
    return red;
};
