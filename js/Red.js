//Redondeo general
var redondear = function(i) {
    return math.round(i, 5);
};

//A degrados
var aDegrados = function(radianes) {
    return (radianes * 180) / math.PI;
};
//A radianes
var aRadianes = function(degrados) {
    return (degrados / 180) * math.PI;
};

var generarTextoReImg = function(P, Q) {
    var texto;
    if (Q === 0) {
        texto = " " + redondear(P);
    } else {
        if (P === 0) {
            texto = " " + redondear(Q) + "j";
        } else {
            var signo = Q < 0 ? "" : "+";
            texto = " " + redondear(P) + " " + signo + redondear(Q) + "j";
        }
    }
    return texto;
};

var generarTextoComplejo = function(complejo) {
    var P = complejo.re;
    var Q = complejo.im;
    var texto;
    if (Q === 0) {
        texto = " " + redondear(P);
    } else {
        if (P === 0) {
            texto = " " + redondear(Q) + "j";
        } else {
            var signo = Q < 0 ? "" : "+";
            texto = " " + redondear(P) + " " + signo + redondear(Q) + "j";
        }
    }
    return texto;
};

var Red = function(red) {

    red.obtenerElmento = function(id) {
        var lista = red.Elementos;
        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.id === id) {
                return elemento;
            }
        }
    };

    red.obtenerPosicionLinea = function(linea, origen, destino) {
        if (origen.tipo === "generador") {
            return {"x": origen.x, "y": origen.y + 30};
        } else if (origen.tipo === "transformador") {
            if (origen.y < destino.y) {
                return {"x": origen.x, "y": origen.y + 20};
            } else {
                return {"x": origen.x, "y": origen.y - 20};
            }
        }
        if (origen.tipo === "barra") {
            var ancho;
            if (destino.y < origen.y) {
                ancho = -5;
            } else {
                ancho = 2;
            }
            if (destino.tipo === "barra") {
                var area = Math.min((origen.x + origen.largo), (destino.x + destino.largo)) -
                        Math.max(origen.x, destino.x);
                linea.x = Math.min(linea.x, area);
                linea.x = Math.max(linea.x, 0);

                if (destino.x < origen.x) {
                    return {"x": origen.x + linea.x, "y": origen.y + ancho};
                } else {
                    return {"x": Math.min(destino.x, origen.x + origen.largo) + linea.x, "y": origen.y + ancho};
                }
            } else {
                if (destino.x < origen.x) {
                    return {"x": origen.x, "y": origen.y + ancho};
                } else {
                    return {"x": Math.min(destino.x, origen.x + origen.largo), "y": origen.y + ancho};
                }
            }
        } else if (origen.tipo === "carga") {
            return {"x": origen.x, "y": origen.y + 15};
        }
        return origen;
    };
    red.obtenerLineas = function(elemento) {
        var connectados = [];
        var lista = red.Elementos;

        for (var index in lista) {
            var linea = lista[index];
            if (linea.tipo === "linea") {
                var origen = this.obtenerElmento(linea.id_origen);
                var destino = this.obtenerElmento(linea.id_destino);
                if (origen === elemento || destino === elemento) {
                    connectados.push(linea);
                }
            }
        }
        return connectados;
    };
    red.obtenerConectados = function(elemento) {
        var connectados = [];
        var lista = red.Elementos;

        for (var index in lista) {
            var linea = lista[index];
            if (linea.tipo === "linea") {
                var origen = this.obtenerElmento(linea.id_origen);
                var destino = this.obtenerElmento(linea.id_destino);
                if (origen === elemento) {
                    connectados.push(destino);
                }
                if (destino === elemento) {
                    connectados.push(origen);
                }
            }
        }
        return connectados;
    };



    red.obtenerConectadosDelMismoTipo = function(elemento, tipo) {
        var connectados = [];
        var lista = red.Elementos;

        for (var index in lista) {
            var linea = lista[index];
            if (linea.tipo === "linea") {
                var origen = this.obtenerElmento(linea.id_origen);
                var destino = this.obtenerElmento(linea.id_destino);
                if (origen === elemento && destino.tipo === tipo) {
                    connectados.push(destino);
                }
                if (destino === elemento && origen.tipo === tipo) {
                    connectados.push(origen);
                }
            }
        }
        return connectados;
    };

    red.obtenerBarrasConnectadas = function(elemento) {
        var connectados = [];
        var lista = red.Elementos;

        for (var index in lista) {
            var linea = lista[index];
            if (linea.tipo === "linea") {
                var origen = this.obtenerElmento(linea.id_origen);
                var destino = this.obtenerElmento(linea.id_destino);
                var barras = [];
                if (origen === elemento && destino.tipo === "barra") {
                    connectados.push(destino);
                }
                if (destino === elemento && origen.tipo === "barra") {
                    connectados.push(origen);
                }
                if (destino === elemento && origen.tipo === "transformador") {
                    barras = red.obtenerBarrasConnectadas(origen);
                }
                if (origen === elemento && destino.tipo === "transformador") {
                    barras = red.obtenerBarrasConnectadas(destino);
                }
                for (var i  in barras) {
                    if (elemento !== barras[i]) {
                        connectados.push(barras[i]);
                    }
                }
            }
        }
        return connectados;
    };

    //se suporque hay una linea entre ambas
    red.obtenerLineaBarras = function(barra1, barra2) {
        var lineas = red.obtenerLineas(barra1);
        for (var i in lineas) {
            var linea = lineas[i];
            if (linea.id_origen === barra1.id && linea.id_destino === barra2.id) {
                return linea;
            }
            if (linea.id_origen === barra2.id && linea.id_destino === barra1.id) {
                return linea;
            }
        }
    };

    //se suporque hay un transformador linea entre ambas
    red.obtenerTransformadorsBarras = function(barra1, barra2) {
        var connectados = red.obtenerConectados(barra1);
        for (var index in connectados) {
            var transf = connectados[index];
            if (transf.tipo === "transformador") {
                var segundosConectados = red.obtenerConectados(transf);
                for (var i in segundosConectados) {
                    var segundosConectado = segundosConectados[i];
                    if (segundosConectado === barra2) {
                        return transf;
                    }
                }
            }
        }
    };
    /**
     * Retorna la l�nea o el transformador
     * @param {type} barra1
     * @param {type} barra2
     * @returns {undefined}R
     */
    red.obtenerEntreBarras = function(barra1, barra2) {
        var connectados = red.obtenerConectados(barra1);
        for (var i in connectados) {
            if (connectados[i] === barra2) {
                return red.obtenerLineaBarras(barra1, barra2);
            }
        }
        return red.obtenerTransformadorsBarras(barra1, barra2);
    };

    red.coordendasLinea = function(linea) {
        var origen = this.obtenerElmento(linea.id_origen);
        var destino = this.obtenerElmento(linea.id_destino);
        var posOrigen = this.obtenerPosicionLinea(linea, origen, destino);
        var posDestino = this.obtenerPosicionLinea(linea, destino, origen);
        return {
            "origen": posOrigen,
            "destino": posDestino,
            "origenX": Math.min(posOrigen.x, posDestino.x) - 20,
            "origenY": Math.min(posOrigen.y, posDestino.y),
            "destinoX": Math.max(posOrigen.x, posDestino.x) + 20,
            "destinoY": Math.max(posOrigen.y, posDestino.y)
        };
    };

    red.obtenerBarraEquilibrio = function() {
        var lista = red.Elementos;
        for (var index in lista) {
            var barra = lista[index];
            if (barra.equilibrio) {
                return barra;
            }
        }
    };

    red.obtenerEquilibrio = function() {
        var total = 0;
        var lista = red.Elementos;
        for (var index in lista) {
            var barra = lista[index];
            if (barra.equilibrio) {
                return total;
            }
            if (barra.tipo === "barra") {
                total++;
            }
        }
        return -1;
    };
    red.totalBarras = function() {
        var total = 0;
        var lista = red.Elementos;
        for (var index in lista) {
            var barra = lista[index];
            if (barra.tipo === "barra") {
                total++;
            }
        }
        return total;
    };

    red.Ve = function() {
        return math.complex({r: red.Configuracion.tensionPU, phi: red.Configuracion.tensionAngulo});
    };

    red.VeVector = function() {
        var largo = red.totalBarras() - 1;
        var VeVector = math.zeros(largo, 1);
        for (var i = 0; i < largo; i++) {
            VeVector._data[i][0] = red.Ve();
        }
        return VeVector;
    };

    red.eliminarElemento = function(elemento) {
        var listaLineas = red.obtenerLineas(elemento);
        for (var index in listaLineas) {
            var linea = listaLineas[index];
            red.eliminarElemento(linea);
        }
        var lista = red.Elementos;
        for (var index in lista) {
            var posible = lista[index];
            if (posible === elemento) {
                lista.splice(index, 1);
            }
        }

    };

    red.estatus = function(elemento) {
        if (elemento.tipo === "linea") {
            return {valido: true};// Las lineas siempre son validas
        }
        var listaConectados = red.obtenerConectados(elemento);
        if (elemento.tipo === "barra") {
            var barrasConnectadas = 0;
            for (var index in listaConectados) {
                var barra = listaConectados[index];
                if (barra.tipo === "barra" || barra.tipo === "transformador") {
                    barrasConnectadas++;
                }
            }
            if (elemento.equilibrio) {
                return {valido: barrasConnectadas >= 2, razon: "La barra de equilibrio no puede estar en una barra radial"};
            } else {
                return {valido: barrasConnectadas >= 1, razon: "Todas las barras tienen que estar conectadas"};
            }
        }
        if (elemento.tipo === "generador" || elemento.tipo === "carga") {
            if (listaConectados.length === 0) {
                return {
                    valido: false,
                    razon: "Todas los elementos deben estar conectados"
                };
            } else if (listaConectados.length > 1) {
                return {
                    valido: false,
                    razon: "Un generador/barras solo puede estar conectador una vez"
                };
            } else {
                var elementoConectado = listaConectados[0];
                if (elementoConectado.tipo === "generador" || elementoConectado.tipo === "carga") {
                    return  {
                        valido: false,
                        razon: "Los generadores y las cargas no pueden estar conectados directamente entre ellos"
                    };
                } else {
                    return {
                        valido: true
                    };
                }
            }
        }
        if (elemento.tipo === "transformador") {
            return {
                valido: listaConectados.length === 2
                , razon: "Un transformadors necesita 2 conexiones"};

        }
        return  {valido: true};
    };

    red.siguienteId = function() {
        return Math.floor((Math.random() * 100000000));
    };

    red.agregarLinea = function(elemento1Id, elmeneto2Id, X, R) {
        var nuevaLinea = {
            "X": X,
            "R": R,
            "x": 0,
            "y": 0,
            "tipo": "linea",
            "id_origen": elemento1Id,
            "id_destino": elmeneto2Id
        };
        red.Elementos.push(nuevaLinea);
    };


    red.agregarElemento = function(tipo, x, y) {
        var nuevoElemento = {
            "x": x,
            "y": y,
            "tipo": tipo,
            "id": red.siguienteId()
        };
        red.Elementos.push(nuevoElemento);
        return nuevoElemento;
    };

    red.obtenerBarra = function(indiceDeseado) {
        var lista = red.Elementos;
        var indiceActual = 0;
        for (var index in lista) {
            var posible = lista[index];
            if (posible.tipo === "barra") {
                if (indiceActual === indiceDeseado) {
                    return posible;
                } else {
                    indiceActual++;
                }
            }
        }
    };

    red.encontrarLinea = function(indiceBarraI, indiceBarraJ) {
        var barraI = red.obtenerBarra(indiceBarraI);
        var barraJ = red.obtenerBarra(indiceBarraJ);
        var lista = red.Elementos;
        for (var index in lista) {
            var posible = lista[index];
            if (posible.tipo === "linea") {
                if (posible.id_origen === barraI.id && posible.id_destino === barraJ.id) {
                    return posible;
                }
            }
        }
        return null;
    };

    red.basePU = function() {
        return red.Configuracion.basePU;
    };

    red.corrienteBase = function() {
        return math.divide(red.Configuracion.basePU,
                math.complex({r: red.Configuracion.tensionPU,
                    phi: red.Configuracion.tensionAngulo}));
    };


    red.removerElementos = function(tipo) {
        var lista = red.Elementos;
        var unaMas;
        do {
            unaMas = false;
            for (var index in lista) {
                var posible = lista[index];
                if (posible.tipo === tipo) {
                    red.eliminarElemento(posible);
                    unaMas = true;
                }
            }
        }while (unaMas);
        return red;
    };


    red.crearCopia = function() {
        return Red(JSON.parse(JSON.stringify(red)));
    };

    red.agregarGeneradorBarraEquilibrio = function(transformador) {
        var barra = red.obtenerBarraEquilibrio();
        var generador = red.agregarElemento("generador", barra.x + barra.largo, barra.y - 100);
        var base = red.Configuracion.basePU;
        generador.P = barra.Se.re - (barra.P * base);
        generador.Q = barra.Se.im - (barra.Q * base);
        generador.V = barra.V;
        generador.Xt = barra.X;
        generador.Xd = barra.equilibrioXd;
        generador.XdPrima = barra.equilibrioXdPrima;
        generador.XdPrimaPrima = barra.equilibrioXdPrimaPrima;
        generador.equilibrio = true;
        barra.equilibrio = false;
        var P = generador.P / base;
        var Q = generador.Q / base;
        var I = generador.corrienteInicial;
        var perdidaTransf = 0;
        if (transformador){
			perdidaTransf =(P*P+Q*Q)*barra.X;
            var transformador = red.agregarElemento("transformador", barra.x + barra.largo, barra.y - 40);
            transformador.S = barra.S;
            transformador.X = barra.X;
            transformador.R = barra.R;
            red.agregarLinea(generador.id, transformador.id);
            red.agregarLinea(transformador.id, barra.id);
        }else {
            red.agregarLinea(generador.id, barra.id);
        }
        generador.Sg = math.complex(P,Q-perdidaTransf);
        return red;
    };
    return red;


};


Red.nuevaRed = function() {
    return Red({
        Configuracion: {
            basePU: 100,
            tensionPU: 1,
            tensionAngulo: 0,
            epsilon: 0.1,
            max_iter: 100
        },
        Elementos: []
    });
};
