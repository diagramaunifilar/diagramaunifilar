/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 * 
 * Carnet A63216
 */

var RegimenPermanente = function(laRed) {
    var red = laRed;
    var equilibrio = red.obtenerEquilibrio();
    var Ve = red.Ve();
    var totalBarras = red.totalBarras();
    var totalBarrasSinEquilibrio = totalBarras - 1;
    var matrizDatosBarra;
    var vectorNombres;
    var YnnInv;
    var Yne;
    var SnConj;
    var Vn;
    var iteracionEstable;
    var Se;
    var YeeConj;
    var voltajes;
    var voltajesConj;
    var admitancia;
    var impedancia;
    var Sij;
    var potenciaComplejaConj;
    var corriente;


    var YnnInv_Yne_Ve;
    var obtenerElmento = function(id) {
        var lista = red.Elementos;
        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.id === id) {
                return elemento;
            }
        }
    };

    var obtenerImpediancia = function(barra1, barra2) {
        if (barra1 === barra2) {
            return{
                "R": 0,
                "X": 0};
        }
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "linea") {
                var origen = obtenerElmento(elemento.id_origen);
                var destino = obtenerElmento(elemento.id_destino);
                if (origen === barra1 || destino === barra1) {
                    if (origen === barra2 || destino === barra2) {
                        return {
                            "X": elemento.X,
                            "R": elemento.R
                        };
                    }
                }
            }
        }
        return {
            "R": 0,
            "X": 0};
    };

    var generarG = function(e) {
        if (e.R === 0 && e.X === 0) {
            return 0;
        }
        return -e.R / (e.R * e.R + e.X * e.X);
    };

    var generarB = function(e) {
        if (e.R === 0 && e.X === 0) {
            return 0;
        }
        return e.X / (e.R * e.R + e.X * e.X);
    };

    var generarNombres = function() {
        var indiceBarra = 0;
        vectorNombres = [];
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                //rellenar matriz
                vectorNombres[indiceBarra] = elemento.nombre;
                indiceBarra++;
            }
        }
    };

    var generarMatrizImpedancia = function() {
        var indiceBarra = 0;
        matrizDatosBarra = [];
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                //rellenar matriz
                matrizDatosBarra[indiceBarra] = [];
                elemento.indiceBarra = indiceBarra;
                indiceBarra++;
            }
        }

        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                for (var index2 in red.Elementos) {
                    var elemento2 = red.Elementos[index2];
                    if (elemento2.tipo === "barra") {
                        matrizDatosBarra[elemento.indiceBarra][elemento2.indiceBarra] = obtenerImpediancia(elemento, elemento2);
                    }
                }
            }
        }
    };


    var agregarAdmitancia = function() {
        for (var index in matrizDatosBarra) {
            for (var index2 in matrizDatosBarra[index]) {
                var elemento = matrizDatosBarra[index][index2];
                elemento.G = generarG(elemento);
                elemento.B = generarB(elemento);
            }
        }
        for (var index in matrizDatosBarra) {
            var G = 0;
            var B = 0;
            for (var index2 in matrizDatosBarra[index]) {
                var elemento = matrizDatosBarra[index][index2];
                B += elemento.B;
                G += elemento.G;
            }
            var elemento = matrizDatosBarra[index][index];
            elemento.G = -G;
            elemento.B = -B;
        }
    };

    var generarMatrizAdmitancia = function() {
        generarMatrizImpedancia();
        agregarAdmitancia();

        admitancia = math.zeros(totalBarras, totalBarras);
        impedancia = math.zeros(totalBarras, totalBarras);
        for (var i = 0; i < totalBarras; i++) {
            for (var j = 0; j < totalBarras; j++) {
                var datosBarra = matrizDatosBarra[i][j];
                impedancia._data[i][j] = math.complex(datosBarra.R, datosBarra.X);
                admitancia._data[i][j] = math.complex(datosBarra.G, datosBarra.B);
            }
        }
    };

    generarYnnInv = function() {
        var Ynn = math.zeros(totalBarrasSinEquilibrio, totalBarrasSinEquilibrio);
        var encontradoI = 0;
        for (var i = 0; i < totalBarras; i++) {
            if (i === equilibrio) {
                encontradoI = 1;
            } else {
                var encontradoJ = 0;
                for (var j = 0; j < totalBarras; j++) {
                    if (j === equilibrio) {
                        encontradoJ = 1;
                    } else {
                        Ynn._data[i - encontradoI][j - encontradoJ] =
                                admitancia._data[i][j];
                    }
                }
            }
        }
        YnnInv = math.inv(Ynn);
        return Ynn;
    };

    var generarYne = function() {
        Yne = math.zeros(totalBarrasSinEquilibrio, 1);
        var encontradoI = 0;
        for (var i = 0; i < totalBarras; i++) {
            if (i === equilibrio) {
                encontradoI = 1;
            } else {
                Yne._data[i - encontradoI][0] =
                        admitancia._data[i][equilibrio];
            }
        }
    };

    var generarSnConj = function() {
        SnConj = math.zeros(red.totalBarras() - 1, 1);
        var lista = red.Elementos;
        var barraActual = 0;
        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.tipo === "barra") {
                if (!elemento.equilibrio) {
                    SnConj._data[barraActual][0] = math.conj(math.complex(elemento.P, elemento.Q));
                    barraActual++;
                }
            }
        }
    };

    var generarPotenciaComplejaConj = function() {
        potenciaComplejaConj = math.zeros(red.totalBarras(), 1);
        var lista = red.Elementos;
        potenciaComplejaConj._data[equilibrio][0] = math.conj(Se);
        var barraActual = 0;
        var equilibrioEncontrado = 0;

        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.tipo === "barra") {
                if (!elemento.equilibrio) {
                    potenciaComplejaConj._data[barraActual + equilibrioEncontrado][0]
                            = SnConj._data[barraActual][0];
                    barraActual++;
                } else {
                    equilibrioEncontrado = 1;
                }
            }
        }
    };

    var generarTexto = function(G, B) {
        if (G === 0) {
            if (B === 0) {
                return "0";
            } else {
                return redondear(B) + " j";
            }
        } else {
            if (B === 0) {
                return redondear(G);
            } else {
                return redondear(G) + " + <br/>" + redondear(B) + " j";
            }
        }
    };


    var generarYnn_Yne_Ve = function() {
        var YnnInv_Yne = math.multiply(YnnInv, Yne);
        YnnInv_Yne_Ve = math.multiply(YnnInv_Yne, Ve);
    };


    this.generarHTMLMatriz = function(matriz) {
        var html = "<tbody><tr><td>Barra</td>";
        for (var fila = 0; fila < totalBarras; fila++) {
            html += ("<td>" + (fila + 1) + " " + vectorNombres[fila] + "</td>");
        }
        html += "</tr>";
        for (var fila = 0; fila < totalBarras; fila++) {
            html += ("<tr><td>" + (fila + 1) + " " + vectorNombres[fila] + "</td>");
            for (var columna = 0; columna < totalBarras; columna++) {
                var barra = matriz._data[fila][columna];
                if (barra === "NoMostrar") {
                    html += ("<td></td>");
                } else {
                    html += ("<td>" + generarTexto(barra.re, barra.im) + "</td>");
                }
            }
            html += "</tr>";
        }
        html += "</tbody>";
        return html;
    };

    this.generarHTMLMatrizConEquilibrio = function(matriz) {
        matriz = matriz._data;
        var html = "<tbody><tr><td>Barra</td>";
        var largo = matriz.length;
        var equiFila = 1;
        for (var fila = 0; fila < largo; fila++) {
            if (fila === equilibrio) {
                equiFila = 2;
            }
            html += ("<td>" + (fila + equiFila) + " " + "</td>");
        }
        html += "</tr>";
        equiFila = 1;
        for (var fila = 0; fila < largo; fila++) {
            if (fila === equilibrio) {
                equiFila = 2;
            }
            html += ("<tr><td>" + (fila + equiFila) + " " + "</td>");
            for (var columna in matriz[fila]) {
                var complejo = matriz[fila][columna];
                html += ("<td>" + generarTexto(complejo.re, complejo.im) + "</td>");
            }
            html += "</tr>";
        }
        html += "</tbody>";
        return html;
    };

    this.generarHTMLVector = function(vector, requireEquilibrio) {
        vector = vector._data;
        var largo = vector.length;
        var equiFila = 1;
        var html = "<tbody><tr><td>Barra</td><td>p.u.</td></tr>";
        equiFila = 1;
        for (var fila = 0; fila < largo; fila++) {
            if (fila === equilibrio && requireEquilibrio) {
                equiFila = 2;
            }
            html += ("<tr><td>" + (fila + equiFila) + " " + vectorNombres[fila + equiFila - 1] + "</td>");

            var complejo = vector[fila][0];
            html += ("<td>" + generarTexto(complejo.re, complejo.im) + "</td>");

            html += "</tr>";
        }
        html += "</tbody>";
        return html;
    };


    var generarNuevoVn = function(Vn) {
        var SnConj_VnConj = math.zeros(totalBarrasSinEquilibrio, 1);
        for (var i = 0; i < totalBarrasSinEquilibrio; i++) {
            SnConj_VnConj._data[i][0] = math.divide(SnConj._data[i][0], math.conj(Vn._data[i][0]));
        }
        var nuevoVn = math.subtract(math.multiply(YnnInv, SnConj_VnConj), YnnInv_Yne_Ve);
        return nuevoVn;
    };

    var aceptable = function(epsilon, nuevoVn, Vn) {
        for (var i = 0; i < totalBarrasSinEquilibrio; i++) {
            if (math.abs(math.subtract(nuevoVn._data[i][0], Vn._data[i][0])) > epsilon) {
                return false;
            }
        }
        return true;
    };


    var generarVn = function() {
        var VeVector = red.VeVector();
        var epsilon = red.Configuracion.epsilon;
        var max_iter = red.Configuracion.max_iter;
        var VnTemp = VeVector;
        var iterId;
        for (iterId = 0; iterId < max_iter; iterId++) {
            var nuevoVn = generarNuevoVn(VnTemp);
            if (aceptable(epsilon, nuevoVn, VnTemp)) {
                VnTemp = nuevoVn;
                iterId++;
                break;
            }
            VnTemp = nuevoVn;
        }
        Vn =  VnTemp;
        iteracionEstable =iterId;

        voltajes = math.zeros(totalBarras, 1);
        var equilibrioEncontrado = 0;
        for (var i = 0; i < totalBarras; i++) {
            if (i === equilibrio) {
                voltajes._data[i][0] = Ve;
                equilibrioEncontrado = 1;
            } else {
                voltajes._data[i][0] = VnTemp._data[i - equilibrioEncontrado][0];
            }
        }
    };

    var generarYeeConj = function() {
        YeeConj = math.conj(admitancia._data[equilibrio][equilibrio]);
    };

    var generarSe = function() {
        var VeConj = math.conj(Ve);
        var VnConj = math.conj(Vn);
        var YenConj = math.transpose(math.conj(Yne));
        Se = math.add(math.multiply(VeConj, math.multiply(YenConj, VnConj)), math.multiply(Ve, math.multiply(YeeConj, VeConj)));
        red.obtenerBarra(equilibrio).Se = math.multiply(Se, red.Configuracion.basePU);
    };

    var generarSij = function() {
        Sij = math.zeros(totalBarras, totalBarras);
        for (var i = 0; i < totalBarras; i++) {
            for (var j = 0; j < totalBarras; j++) {
                var Zij = impedancia._data[i][j];
                if (Zij.toPolar().r !== 0) {
                    var Vi = voltajes._data[i][0];
                    var Vj = voltajes._data[j][0];

                    var conj = math.multiply(Vi, math.conj(math.divide(math.subtract(Vi, Vj), Zij)));
                    //console.log(math.divide(math.subtract(Vi, Vj), Zij));
                    Sij._data[i][j] = math.multiply(conj, red.Configuracion.basePU);
                } else {
                    Sij._data[i][j] = "NoMostrar";
                }
                if (Sij._data[i][j] !== "NoMostrar") {
                    var linea = red.encontrarLinea(i, j);
                    if (linea !== null) {
                        linea.Sij = Sij._data[i][j];
                    }
                }
            }
        }
    };

    var generarCorriente = function() {
        corriente = math.zeros(totalBarras, 0);
        for (var i = 0; i < totalBarras; i++) {
            corriente._data[i][0] =
                    math.divide(potenciaComplejaConj._data[i][0], voltajesConj._data[i][0]);
        }
    };

    this.perdidasPotencia = function(){
        var perdidas = math.complex(0,0);
        for (var index =0; index < totalBarras;index++) {
            perdidas = math.add(perdidas,math.conj(potenciaComplejaConj._data[index][0]));
        }
        return perdidas;
    };
    
    this.YnnInv = function() {
        return YnnInv;
    };

    this.Yne = function() {
        return Yne;
    };



    this.SnConj = function() {
        return SnConj;
    };

    this.Se = function() {
        return Se;
    };

    this.Vn = function() {
        return Vn;
    };

    this.iteracionEstable = function() {
        return iteracionEstable;
    };

    this.admitancia = function() {
        return admitancia;
    };

    this.impedancia = function() {
        return impedancia;
    };

    this.voltajesConj = function() {
        return voltajesConj;
    };

    this.Sij = function() {
        return Sij;
    };

    this.potenciaComplejaConj = function() {
        return potenciaComplejaConj;
    };



    this.corriente = function() {
        return corriente;
    };

    


    generarNombres();
    generarMatrizAdmitancia();
    generarYnnInv();
    generarYne();
    generarSnConj();
    generarYnn_Yne_Ve();
    generarVn();
    generarYeeConj();
    generarSe();
    generarSij();
    generarPotenciaComplejaConj();
    voltajesConj = math.conj(voltajes);
    generarCorriente();


};
