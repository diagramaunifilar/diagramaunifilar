/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var Simplificar = function(redOriginal) {

    var red = redOriginal;
    var redes = [];

    var encontrarBarrasRadiales = function(mired) {
        var lista = mired.Elementos;
        var unaMas;
        do {
            unaMas = false;
            for (var index in lista) {
                var elemento = lista[index];
                if (elemento.tipo === "barra" && !elemento.radial) {
                    var conectados = mired.obtenerBarrasConnectadas(elemento);
                    if (conectados.length === 1) {
                        elemento.radial = true;
                        unaMas = true;
                    } else {
                        var barrasNoRadiales = 0;
                        for (var i in conectados) {
                            var barra = conectados[i];
                            if (!barra.radial) {
                                barrasNoRadiales++;
                            }
                        }
                        if (barrasNoRadiales === 1) {
                            elemento.radial = true;
                            unaMas = true;
                        }
                    }
                }
            }
        } while (unaMas);
        redes.push(mired);
        return mired;
    };

    var encontrarCorrienteOriginal = function(mired) {
        var lista = mired.Elementos;
        for (var index in lista) {
            var Ve = red.Ve();
            var pu = red.basePU();
            var elemento = lista[index];
            if (elemento.tipo === "generador" || elemento.tipo === "carga") {
                var P = elemento.P / pu;
                var Q =elemento.Q / pu;
                var I = math.divide(math.complex(P, -Q), Ve);
                if (elemento.tipo === "carga") {
                    I = math.multiply(I, -1);
                }else {
					var conectados = mired.obtenerConectados(elemento);
					var perdidaTransf = 0;
					for (var i in conectados) {
						if (conectados[i].tipo === 'transformador') {
							perdidaTransf =(P*P+Q*Q)*conectados[i].X;
						}
                	}
                	elemento.Sg = math.complex(P,Q-perdidaTransf);
				}
                elemento.corrienteInicial = I;
            }
        }
        redes.push(mired);
        return mired;
    };

    var encontrarCorrienteOriginalTransf = function(mired) {
        var lista = mired.Elementos;
        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.tipo === "transformador") {
                var conectados = mired.obtenerConectados(elemento);
                for (var i in conectados) {
                    if (typeof conectados[i].corrienteInicial !== 'undefined') {
                        elemento.corrienteInicial = conectados[i].corrienteInicial;
                    }
                }
            }
        }
        return mired;
    };

    var encontrarCorrienteOriginalBarras = function(mired) {
        var lista = mired.Elementos;
        var unaMas = true;
        while (unaMas) {
            unaMas = false;
            for (var index in lista) {
                var elemento = lista[index];
                if (elemento.tipo === "barra" &&
                        typeof elemento.corrienteInicial === 'undefined' &&
                        elemento.radial) {
                    var conectados = mired.obtenerConectados(elemento);
                    var corrienteInicial = math.complex(0, 0);
                    var corrientesNoEncontradas = 0;
                    for (var i in conectados) {
                        if (typeof conectados[i].corrienteInicial !== 'undefined') {
                            corrienteInicial = math.add(corrienteInicial, math.complex(conectados[i].corrienteInicial));
                        } else {
                            corrientesNoEncontradas++;
                        }
                    }
                    if (corrientesNoEncontradas > 1) {
                        unaMas = true;
                    } else {
                        elemento.corrienteInicial = corrienteInicial;
                    }
                }
            }
        }
        redes.push(mired);
        return mired;
    };

    var encontrarPerdidaConCorriente = function(corrienteInicial, elemento) {
        var I = math.complex(corrienteInicial);
        var absI = I.toPolar().r;
        var I2 = absI * absI;
        return {
            P: elemento.R * I2,
            Q: elemento.X * I2
        };
    };

    var removerTransformadores = function(mired) {
        var basePU = mired.basePU();
        var lista = mired.Elementos;
        var unaMas = true;
        while (unaMas) {
            unaMas = false;
            for (var index in lista) {
                var elemento = lista[index];
                if (elemento.tipo === "transformador" && typeof elemento.corrienteInicial !== 'undefined') {
                    var barra;
                    var generadorOcarga;
                    var conectados = mired.obtenerConectados(elemento);
                    if (conectados[0].tipo === "barra") {
                        barra = conectados[0];
                        generadorOcarga = conectados[1];
                    } else {
                        barra = conectados[1];
                        generadorOcarga = conectados[0];
                    }
                    mired.eliminarElemento(elemento);
                    mired.agregarLinea(conectados[0].id, conectados[1].id, elemento.X, elemento.R);
                    var carga = mired.agregarElemento("carga", elemento.x + 40, elemento.y);
                    var PQ = encontrarPerdidaConCorriente(elemento.corrienteInicial, elemento);
                    carga.P = PQ.P * basePU;
                    carga.Q = PQ.Q * basePU;
                    mired.agregarLinea(barra.id, carga.id);
                    unaMas = true;
                }
            }
        }
        redes.push(mired);
        return mired;
    };

    var removerTransformadoresEntreBarras = function(mired) {
        var lista = mired.Elementos;
        for (var index in lista) {
            var elemento = lista[index];
            delete elemento.corrienteInicial;
            if (elemento.tipo === "transformador") {
                var conectados = mired.obtenerConectados(elemento);
                mired.agregarLinea(conectados[0].id, conectados[1].id, elemento.X, elemento.R);
                mired.eliminarElemento(elemento);
                index = 0;
            }
            if (elemento.tipo === "generador") {
                elemento.dibujarGenerador = 0;
            }
        }
        redes.push(mired);
        return mired;
    };
    var removerBarras = function(mired) {
        var basePU = mired.basePU();
        var lista = mired.Elementos;
        var unaMas = true;
        while (unaMas) {
            unaMas = false;
            for (var index in lista) {
                var elemento = lista[index];
                if (elemento.tipo === "barra" &&
                        elemento.radial) {
                    var barras = mired.obtenerBarrasConnectadas(elemento);
                    if (barras.length === 1) {
                        unaMas = true;
                        var barra = barras[0];
                        var conectados = mired.obtenerConectados(elemento);

                        for (var i in conectados) {
                            if (conectados[i].tipo !== "barra") {
                                mired.agregarLinea(conectados[i].id, barra.id);
                            }
                        }
                        var entreAmbos = mired.obtenerEntreBarras(elemento, barra);
                        var carga = mired.agregarElemento("carga", elemento.x + 40, elemento.y);
                        var PQ = encontrarPerdidaConCorriente(elemento.corrienteInicial, entreAmbos);
                        carga.P = PQ.P * basePU;
                        carga.Q = PQ.Q * basePU;
                        mired.agregarLinea(barra.id, carga.id);
                        mired.eliminarElemento(elemento);
                        if (entreAmbos.tipo === "transformador") {
                            mired.eliminarElemento(entreAmbos);
                        }
                        unaMas = true;
                    }
                }
            }
        }
        redes.push(mired);
        return mired;
    };

    var unirGeneradoresCargasAux = function(mired, barra, tipo) {
        var lista = mired.obtenerConectadosDelMismoTipo(barra, tipo);
        var largo = lista.length;
        if (largo > 0) {
            var generadorOCarga = lista[0];
            generadorOCarga.x = Math.max(generadorOCarga.x, barra.x);
            if (largo > 1) {
                var index = 1;
                while (index < largo) {
                    generadorOCarga.P += lista[index].P;
                    generadorOCarga.Q += lista[index].Q;
                    mired.eliminarElemento(lista[index]);
                    index++;
                }
                return true;
            } else {
                return false;
            }
        }
    };

    var unirGenCargas = function(mired) {
        var unaMas;
        do {
            unaMas = false;
            for (var index in mired.Elementos) {
                var elemento = mired.Elementos[index];
                if (elemento.tipo === "barra") {
                    if (unirGeneradoresCargasAux(mired, elemento, "carga") |
                            unirGeneradoresCargas(mired, elemento, "generador")) {
                        unaMas = true;
                    }
                }
            }
        } while (unaMas);
        redes.push(mired);
        return mired;
    };


    var agregarXt = function(mired) {
        var lista = mired.Elementos;
        for (var index in lista) {
            var elemento = lista[index];
            if (elemento.tipo === "generador") {
                var conectado = mired.obtenerConectados(elemento)[0];
                var barra = mired.obtenerBarrasConnectadas(elemento)[0];
                if (conectado.tipo === "transformador"){

						elemento.Xt  = conectado.X;

                }else {
                    elemento.Xt =0;
                }
            }
        }
    }

    var simplificarRed = function() {
        var miRed = encontrarBarrasRadiales(red.crearCopia());
        //Si existe Xt al generador
        agregarXt(miRed);
        miRed = encontrarCorrienteOriginal(miRed.crearCopia());
        miRed = encontrarCorrienteOriginalTransf(miRed.crearCopia());
        miRed = encontrarCorrienteOriginalBarras(miRed.crearCopia());
        miRed = removerTransformadores(miRed.crearCopia());
        if($("#simplificar_barras").prop('checked')){
			miRed = removerBarras(miRed.crearCopia());
		}
        miRed = unirGenCargas(miRed.crearCopia());
        miRed = removerTransformadoresEntreBarras(miRed.crearCopia());
        return miRed;
    };


    var unirGeneradoresCargas = function(barra, tipo) {
        var lista = red.obtenerConectadosDelMismoTipo(barra, tipo);
        var largo = lista.length;
        if (largo > 0) {
            var generadorOCarga = lista[0];
            generadorOCarga.x = Math.max(generadorOCarga.x, barra.x);
            if (largo > 1) {
                var index = 1;
                while (index < largo) {
                    generadorOCarga.P += lista[index].P;
                    generadorOCarga.Q += lista[index].Q;
                    red.eliminarElemento(lista[index]);
                    index++;
                }
                return true;
            } else {
                return false;
            }
        }
    };



    redes.push(red);
    simplificarRed();
    return redes;
};
