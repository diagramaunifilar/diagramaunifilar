/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 * 
 * Carnet A63216
 */

var Simplificar = function(redOriginal) {
    var red = Red(JSON.parse(JSON.stringify(redOriginal)));


    var unirGeneradoresCargas = function(barra, tipo) {
        var lista = red.obtenerConectadosDelMismoTipo(barra, tipo);
        var largo = lista.length;
        if (largo > 0) {
            var generadorOCarga = lista[0];
            generadorOCarga.x = Math.max(generadorOCarga.x, barra.x);
            if (largo > 1) {
                var index = 1;
                while (index < largo) {
                    generadorOCarga.P += lista[index].P;
                    generadorOCarga.Q += lista[index].Q;
                    red.eliminarElemento(lista[index]);
                    index++;
                }
                return true;
            } else {
                return false;
            }
        }

    };

    /**
     * Encuentra la perdida causada por transformador
     * entre la barra y el generador or carga
     * @param {type} generador
     * @param {type} transformador
     * @returns {Simplificar.encontrarPerdida.perdida}
     */
    var encontrarCorrienteYPerdidas = function(generador, transformador) {
        var basePU = red.Configuracion.basePU;
        var anguloRadianes = red.Configuracion.tensionAngulo * (math.pi / 180.0);
        var S_barra_pu = math.complex({re: generador.P / basePU, im: generador.Q / basePU});
        var V_barra_pu = math.complex({r: red.Configuracion.tensionPU, phi: anguloRadianes});
        var I_barra_pu = math.divide(S_barra_pu, V_barra_pu);
        var conj_vect_I_barra_pu = math.conj(I_barra_pu);
        var abs_vect_I_barra_pu = conj_vect_I_barra_pu.toPolar().r;
        var abs_vect_I_barra_pu = abs_vect_I_barra_pu * abs_vect_I_barra_pu;
        var perdidasReactivas = abs_vect_I_barra_pu * transformador.X;
        var perdidasReactivas_pu = perdidasReactivas * basePU;
        return {
            perdidasReactivas: perdidasReactivas_pu,
            corriente: I_barra_pu
        }
    };

    var borrarTransformadores = function(){
        //Primero trato de borrar todos los transformadores
        var transformadorEncontrado;
        do {
            transformadorEncontrado =false;
            for (var index in red.Elementos) {
                var elemento = red.Elementos[index];
                if (elemento.tipo === "transformador") {
                    transformadorEncontrado = true;
                    var conectados = red.obtenerConectados(elemento);
                    var entreBarras;
                    red.eliminarElemento(elemento);
                    if (conectados[0].tipo === "barra" && conectados[1].tipo === "barra") {
                        entreBarras = true;
                    } else {
                        entreBarras = false;
                    }
                    red.agregarLinea(conectados[0].id, conectados[1].id, elemento.X, elemento.R);
                    if (!entreBarras) {
                        var barra;
                        var generador;
                        if (conectados[0].tipo === "barra") {
                            barra = conectados[0];
                            generador = conectados[1];
                        } else {
                            barra = conectados[1];
                            generador = conectados[0];
                        }
                        var carga = red.agregarElemento("carga", elemento.x + 40, elemento.y);
                        carga.P = 0;
                        var corrienteYPerdidas = encontrarCorrienteYPerdidas(generador, elemento);
                        carga.Q = corrienteYPerdidas.perdidasReactivas;
                        barra.corriente = corrienteYPerdidas.corriente;
                        red.agregarLinea(barra.id, carga.id);
                    }
                    break;
                }
            }
        } while (transformadorEncontrado);
    };
    
    
    var simplificarRedAux = function() {    
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                if (red.obtenerConectadosDelMismoTipo(elemento, "barra").length === 1) {
                    var conectados = red.obtenerConectados(elemento);
                    red.eliminarElemento(elemento);
                    for (var i in conectados) {
                        if (conectados[i].tipo === "barra") {
                            var barraQuePermanece = conectados[i];
                            for (var j in conectados) {
                                if (i !== j) {
                                    red.agregarLinea(barraQuePermanece.id, conectados[j].id);
                                }
                            }

                        }
                    }
                    return;
                }
            }
        }
    };
    
    var simplificarRed = function() {
        borrarTransformadores();
        var totalBarras;
        while (totalBarras !== red.totalBarras()) {
            totalBarras = red.totalBarras();
            simplificarRedAux();

        }
        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "barra") {
                if (unirGeneradoresCargas(elemento, "carga") |
                        unirGeneradoresCargas(elemento, "generador")) {
                    index = 0;
                }
            }
        }

        for (var index in red.Elementos) {
            var elemento = red.Elementos[index];
            if (elemento.tipo === "generador") {
                elemento.dibujarGenerador = false;
            }
            if (elemento.tipo === "carga") {
                elemento.dibujarBarraCarga = false;
            }
        }
    };
    simplificarRed();
    return red;
};
