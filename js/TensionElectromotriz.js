/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 *
 * Carnet A63216
 */

var TensionElectromotriz = function(redOriginal){
    var red = redOriginal.crearCopia();
    var encontrarTensionElectromotriz = function() {
        var lista = red.Elementos;
        for (var index in lista) {
            var generador = lista[index];
            if (generador.tipo === "generador") {
                var V= math.complex( generador.V);
                var Sg = math.complex(generador.Sg);
                var I = math.divide( math.conj(Sg),math.conj(V));
                var Xt = generador.Xt;
                generador.E =math.add(V,math.multiply(I,math.complex(0,Xt+(generador.Xd/100.0))));
                generador.EPrima=math.add(V,math.multiply(I,math.complex(0,Xt+(generador.XdPrima/100.0))));
                generador.EPrimaPrima =math.add(V,math.multiply(I,math.complex(0,Xt+(generador.XdPrimaPrima/100.0))));
            }
        }
    };
    encontrarTensionElectromotriz();
    return red;
};
