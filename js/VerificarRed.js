/*
 * Creado por Maria del Mar Herrera Herrera
 * Como proyecto UCR
 * 
 * Carnet A63216
 */
//Verifica si la red es valida
var VerificarRed = function(red,simplificada) {
    var status;
    var verificarRed = function( ) {
        var lista = red.Elementos;
        for (var index in lista) {
            var elemStatus = red.estatus(lista[index]);
            if (!elemStatus.valido){
                status = elemStatus;
                return;
            }
        }
        if (red.obtenerEquilibrio() === -1){
            if (simplificada){
                status = {valido:false,razon:"La barra de equilibrio no puede estar en una barra radial"};
            }else {
                status = {valido:false,razon:"No se ha definido barra de equilibrio"};
            }
        }else {
            status = {valido:true};
        }
        
    };

    verificarRed();
    return status;
};
